var gulp = require("gulp");
var sass = require("gulp-sass");
var autoprefixer = require("gulp-autoprefixer");
var uglify = require("gulp-uglify");
var browser = require("browser-sync");
var plumber = require("gulp-plumber");

gulp.task("server", function() {
    browser({
        server: {
            baseDir: "./html"
        }
    });
});

gulp.task("sass", function() {
    gulp.src("html/assets/sass/**/*scss")
        .pipe(plumber())
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest("./html/assets/css"))
        .pipe(browser.reload({stream:true}))
});

gulp.task("js", function() {
    gulp.src(["html/assets/js/**/*.js","!html/assets/js/min/**/*.js"])
        .pipe(uglify())
        .pipe(gulp.dest("./html/assets/js/min"))
        .pipe(browser.reload({stream:true}))
});

gulp.task("html", function() {
    gulp.src(["html/**/*.html"])
        .pipe(browser.reload({stream:true}))
});

gulp.task("default",['server'], function() {
    gulp.watch(["html/assets/js/**/*.js","!html/assets/js/min/**/*.js"],["js"]);
    gulp.watch("html/assets/sass/**/*.scss",["sass"]);
    gulp.watch("html/**/*.html",["html"]);
});